package ru.kbinform.artistinform.utils;

/**
 * All genres found on yandex.music
 */
public enum Genres {
    pop, indie, rock, metal, alternative, electronics, electronic, dance, rap, hip_hop, rnb, r_n_b, jazz, blues,
    reggae, ska, punk, folk, world, classical, estrada, shanson, country, soundtrack, relax, easy, bard,
    singer_songwriter, children, other, disco, local_indie, rusrock, rnr, rock_n_roll, prog, prog_rock, postrock,
    post_rock, newwave, new_wave, ukrrock, dubstep, industrial, experimental, house, techno, trance, dnb, drum_n_bass,
    rusrap, urban, soul, funk, tradjazz, trad_jass, conjazz, modern_jazz, reggaeton, dub, hardcore, rusfolk, russian,
    tatar, celtic, balkan, eurofolk, european, jewish, eastern, african, latinfolk, latin_american, amerfolk,
    american, vocal, opera, modern, modern_classical, films, tvseries, tv_series, animated, animated_films,
    videogame, videogame_music, musical, bollywood, lounge, newage, new_age, meditation, meditative, rusbards,
    romances, sport, holiday;
}
