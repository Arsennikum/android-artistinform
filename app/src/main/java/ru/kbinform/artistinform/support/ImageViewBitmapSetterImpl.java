package ru.kbinform.artistinform.support;

import android.graphics.Bitmap;
import android.widget.ImageView;
import ru.kbinform.artistinform.R;
import ru.kbinform.artistinform.interfaces.ImageViewBitmapSetter;

/**
 * Устанавливает картинку в заданный imageView,
 * или ставит картинку с сообщением, что загрузить не удалось.
 */
public class ImageViewBitmapSetterImpl implements ImageViewBitmapSetter {
    private ImageView imageView;

    public ImageViewBitmapSetterImpl(ImageView imageView) {
        this.imageView = imageView;
    }

    @Override
    public void displayInImageView(Bitmap bitmap) {
        imageView.setImageBitmap(bitmap);
    }

    @Override
    public void displayNotDownloaded() {
        imageView.setImageResource(R.drawable.image_not_downloaded);
        imageView.setMaxWidth(100);
    }
}
