package ru.kbinform.artistinform.support;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import ru.kbinform.artistinform.interfaces.ImageViewBitmapSetter;

import java.io.IOException;
import java.net.URL;

/**
 * For downloading image.
 * In this project for download covers.
 */
public class ImageDownloader extends AsyncTask<URL, Void, Bitmap> {
    private ImageViewBitmapSetter imageViewSetter;
    /**
     * If sets, downloaded Bitmap sets to this ImageView
     */
    public void setImageViewSetter(ImageViewBitmapSetter imageViewSetter) {
        this.imageViewSetter = imageViewSetter;
    }

    @Override
    protected Bitmap doInBackground(URL... params) {
        try {
            URL coverSmallUrl = params[0];
            return BitmapFactory.decodeStream(coverSmallUrl.openStream());
        } catch (IOException e) {
            return null;
        }
    }

    @Override
    protected void onPostExecute(Bitmap bitmap) {
        super.onPostExecute(bitmap);
        if (imageViewSetter != null) {
            if (bitmap != null) {
                imageViewSetter.displayInImageView(bitmap);
            }
            else {
                imageViewSetter.displayNotDownloaded();
            }
        }
    }
}