package ru.kbinform.artistinform.interfaces;

import java.util.ArrayList;

/**
 * For link listView and data to display on it
 */
public interface LoadedDataHandler<Data> {
    /**
     * Displaying data in listView
     * @param data data to display
     */
    void fillListView(ArrayList<Data> data);

    /**
     * Use to displaying msg that data not been downloaded
     */
    void cantLoadData();
}
