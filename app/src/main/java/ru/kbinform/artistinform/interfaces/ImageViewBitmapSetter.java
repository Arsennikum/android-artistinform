package ru.kbinform.artistinform.interfaces;

import android.graphics.Bitmap;

/**
 * Интерфейс для установки картинки в заданный ImageView.
 */
public interface ImageViewBitmapSetter {
    /**
     * Устанавливает Bitmap в заданных ImageView
     * @param bitmap изображение, которое необходимо поставить в ImageView
     */
    void displayInImageView(Bitmap bitmap);

    /**
     * Выполняет действия в случае неудачной загрузки картинки
     */
    void displayNotDownloaded();
}
