package ru.kbinform.artistinform.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import ru.kbinform.artistinform.R;

public class ReloadRequestActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reload_request);
    }

    public void onReloadBtnClick(View view) {
        setResult(RESULT_OK);
        finish();
    }
}
