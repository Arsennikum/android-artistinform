package ru.kbinform.artistinform.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;
import ru.kbinform.artistinform.R;
import ru.kbinform.artistinform.core.Artist;
import ru.kbinform.artistinform.core.ArtistParserTask;
import ru.kbinform.artistinform.core.ArtistViewAdapter;
import ru.kbinform.artistinform.interfaces.LoadedDataHandler;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class CatalogActivity extends AppCompatActivity
        implements AdapterView.OnItemClickListener, LoadedDataHandler<Artist> {
    private ListView artistsView;
    private ArrayList<Artist> artists;
    private int artistsViewPosition;

    @Override
    public void fillListView(ArrayList<Artist> data) {
        // Вначале удаляем анимацию загрузки
        ProgressBar progressBar = (ProgressBar)findViewById(R.id.progress_bar);
        if (progressBar != null) {
            ((ViewGroup) progressBar.getParent()).removeView(progressBar);
        }

        // А затем заполняем ListView
        this.artists = data;
        ArtistViewAdapter adapter = new ArtistViewAdapter(this,
                R.layout.artist_list_item, artists);
        artistsView.setAdapter(adapter);
    }
    @Override
    public void cantLoadData() {
        Intent intent = new Intent(this, ReloadRequestActivity.class);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // В любом случае пытаемся перезагрузить
        tryToLoadData();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_catalog);

        setTitle(R.string.catalog_activity_title);
        artistsView = (ListView)findViewById(R.id.artist_list);
        artistsView.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (artists == null) {
            tryToLoadData();
        } else {
            fillListView(artists);
            artistsView.setSelection(artistsViewPosition);
        }
    }

    /**
     * Парсинг и отображение исполнителей
     */
    public void tryToLoadData() {
        ArtistParserTask artistParserTask;
        try {
            artistParserTask = new ArtistParserTask();
            artistParserTask.setLoadedDataHandler(this);
            artistParserTask.execute(new URL(getString(R.string.data_url)));
        } catch (MalformedURLException e) {
            // В случае невозможности распарсить URL, жестко заданный в ресурсах. По идее быть не может
            e.printStackTrace();
            Toast.makeText(this, "Критическая ошибка. Обратитесь к разработчику", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Artist artist = artists.get(position);
        artistsViewPosition = position;
        // display selected artist in new activity
        Intent intent = new Intent(this, ArtistDescriptionActivity.class);
        intent.putExtra("selectedArtist", artist);
        startActivity(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelableArrayList("artists", artists);
        outState.putParcelable("artistViewState", artistsView.onSaveInstanceState());
        outState.putInt("artistsViewFirstVisiblePosition", artistsView.getFirstVisiblePosition());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        artists = savedInstanceState.getParcelableArrayList("artists");
        artistsView.onRestoreInstanceState(savedInstanceState.getParcelable("artistViewState"));
        artistsViewPosition = savedInstanceState.getInt("artistsViewFirstVisiblePosition");
        savedInstanceState.clear();
    }
}
