package ru.kbinform.artistinform.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;
import ru.kbinform.artistinform.R;
import ru.kbinform.artistinform.core.Artist;
import ru.kbinform.artistinform.interfaces.ImageViewBitmapSetter;
import ru.kbinform.artistinform.support.ImageDownloader;
import ru.kbinform.artistinform.support.ImageViewBitmapSetterImpl;

import java.io.IOException;
import java.net.URL;

public class ArtistDescriptionActivity extends AppCompatActivity {
    TextView genres, stats, description;
    ImageView coverBig;
    Artist artist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_description);
        genres = (TextView) findViewById(R.id.genres);
        stats = (TextView) findViewById(R.id.stats);
        description = (TextView) findViewById(R.id.description);
        coverBig = (ImageView) findViewById(R.id.coverBig);
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (artist == null) {
            artist = getIntent().getParcelableExtra("selectedArtist");
        }
        fillData();
    }

    private void fillData() {
        setTitle(artist.getName());
        genres.setText(artist.getAllGenres());
        String statsStr = artist.getAlbumsCount() + " альбомов, " + artist.getTracksCount() + " песен.";
        stats.setText(statsStr);
        description.setText(artist.getDescription());
        // setting cover in this try{}
        ImageViewBitmapSetter imageViewBitmapSetter = new ImageViewBitmapSetterImpl(coverBig);
        try {
            URL coverUrl = new URL(artist.getCoverBigLink());
            ImageDownloader imageDownloader = new ImageDownloader();

            imageDownloader.setImageViewSetter(imageViewBitmapSetter);
            imageDownloader.execute(coverUrl);
        } catch (IOException e) {
            imageViewBitmapSetter.displayNotDownloaded();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putParcelable("artist", artist);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        artist = savedInstanceState.getParcelable("artist");
    }
}
