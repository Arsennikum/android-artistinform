package ru.kbinform.artistinform.core;

import android.os.AsyncTask;
import android.util.Log;
import org.json.JSONArray;
import org.json.JSONException;
import ru.kbinform.artistinform.activities.CatalogActivity;
import ru.kbinform.artistinform.interfaces.LoadedDataHandler;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Download JSON and parsing artists from yandex JSON artists model.
 * URL with JSON for parse in Execute() parameter
 */
public class ArtistParserTask extends AsyncTask<URL, Void, String>{
    private CatalogActivity catalogActivity;
    private static final String LOG_TAG = "ArtistParserTask";
    private LoadedDataHandler<Artist> dataLinker;

    public void setLoadedDataHandler(LoadedDataHandler<Artist> dataLinker) {
        this.dataLinker = dataLinker;
    }

    @Override
    protected String doInBackground(URL... params) {
        // Загрузка JSON по переданному URL
        HttpURLConnection urlConnection = null;
        BufferedReader reader = null;
        String receivedJson = "";
        try {
            urlConnection = (HttpURLConnection) params[0].openConnection();
            urlConnection.setRequestMethod("GET");
            urlConnection.connect();

            InputStream inputStream = urlConnection.getInputStream();
            StringBuilder inputBuffer = new StringBuilder();

            reader = new BufferedReader(new InputStreamReader(inputStream));

            String receivedLine;
            while ((receivedLine = reader.readLine()) != null) {
                inputBuffer.append(receivedLine);
            }

            receivedJson = inputBuffer.toString();
        }
        catch (ProtocolException e) {
            Log.d(LOG_TAG, "doInBackground protocolException");
            e.printStackTrace();
        }
        catch (IOException e) {
            Log.d(LOG_TAG, "doInBackground IOException");
            e.printStackTrace();
        }

        return receivedJson;
    }

    @Override
    protected void onPostExecute(String inputJson) {
        super.onPostExecute(inputJson);

        JSONArray jsonArtistsArray;
        ArrayList<Artist> artists;
        try {
            jsonArtistsArray = new JSONArray(inputJson);
            artists = Artist.parseToListFromJsonArray(jsonArtistsArray);
            if (dataLinker != null) {
                dataLinker.fillListView(artists);
            }
        }
        catch (JSONException e) {
            Log.d(LOG_TAG, "onPostExecute JSONException");
            e.printStackTrace();
            if (dataLinker != null) {
                dataLinker.cantLoadData();
            }
        }
    }
}
