package ru.kbinform.artistinform.core;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import ru.kbinform.artistinform.R;
import ru.kbinform.artistinform.interfaces.ImageViewBitmapSetter;
import ru.kbinform.artistinform.support.ImageDownloader;
import ru.kbinform.artistinform.support.ImageViewBitmapSetterImpl;

import java.io.IOException;
import java.net.URL;
import java.util.List;

/**
 * Created by Алексей on 09.05.2016.
 */
public class ArtistViewAdapter extends ArrayAdapter<Artist> {
    private List<Artist> artists;
    private Context context;
    private ImageView coverSmall;

    public ArtistViewAdapter(Context context, int resource, List<Artist> artists) {
        super(context, resource);
        this.context = context;
        this.artists = artists;
    }

    @Override
    public Artist getItem(int posititon) {
        return artists.get(posititon);
    }

    @Override
    public int getCount() {
        return artists.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // Задание вида элемента списка, созданного выше
        View view = inflater.inflate(R.layout.artist_list_item, parent, false); // TODO реализовать ViewHolder, о котором говорит IDEA http://stackoverflow.com/questions/29230403/should-i-use-view-holder-pattern-for-my-adapter

        // проставляем данные для элементов.
        TextView nickName = (TextView)view.findViewById(R.id.title);
        TextView genres = (TextView)view.findViewById(R.id.genres);
        TextView stats = (TextView)view.findViewById(R.id.stats);
        coverSmall = (ImageView)view.findViewById(R.id.coverSmall);

        Artist artist = artists.get(position);

        nickName.setText(artist.getName());
        genres.setText(artist.getAllGenres());
        String statsStr = artist.getAlbumsCount() + " альбомов, " + artist.getTracksCount() + " песен."; // TODO: строки в ресурсы, погуглить решения для склонений
        stats.setText(statsStr);

        // Ставим cover картинку
        ImageViewBitmapSetter imageViewBitmapSetter = new ImageViewBitmapSetterImpl(coverSmall);
        try {
            URL coverUrl = new URL(artist.getCoverSmallLink());
            ImageDownloader imageDownloader = new ImageDownloader();
            imageDownloader.setImageViewSetter(imageViewBitmapSetter);
            imageDownloader.execute(coverUrl);
        } catch (IOException e) {
            imageViewBitmapSetter.displayNotDownloaded();
        }
        return view;
    }
}
