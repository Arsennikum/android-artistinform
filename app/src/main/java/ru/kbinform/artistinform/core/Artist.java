package ru.kbinform.artistinform.core;

import android.os.Parcel;
import android.os.Parcelable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import ru.kbinform.artistinform.support.Genres;

import java.util.ArrayList;
import java.util.List;

/**
 * Presenting Artist from received yandex.music json object
 */
public class Artist implements Parcelable {
    private final int id;
    private final String name, description;
    private final String officialLink, coverSmallLink, coverBigLink;
    private final int tracksCount, albumsCount;
    private final List<Genres> genres;
    /**
     * For genres, which not in enum Genres (if inputJson contains such as).
     */
    private final List<String> unknownGenres;

    public Artist(JSONObject inputJson) throws JSONException {
        id              = inputJson.optInt("id", -1);
        name            = inputJson.optString("name");
        description     = inputJson.optString("description");
        officialLink    = inputJson.optString("link");
        tracksCount     = inputJson.optInt("tracks", -1);
        albumsCount     = inputJson.optInt("albums", -1);
        JSONObject coverLinks;
        coverLinks      = inputJson.optJSONObject("cover");
        if (coverLinks != null) {
            coverSmallLink  = coverLinks.getString("small");
            coverBigLink    = coverLinks.getString("big");
        } else {
            coverSmallLink  = "";
            coverBigLink    = "";
        }

        JSONArray genresJson = inputJson.getJSONArray("genres");
        genres = new ArrayList<>(genresJson.length());
        unknownGenres = new ArrayList<>();

        for (int i = 0; i < genresJson.length(); i++) {
            String genre = genresJson.getString(i);
            genre = genre.replace('-', '_');
            try {
                genres.add(Genres.valueOf(genre));
            } catch (IllegalArgumentException e) {
                // Если в enum нет этого жанра
                unknownGenres.add(genre);
            }
        }
    }

    static public ArrayList<Artist> parseToListFromJsonArray(JSONArray from) throws JSONException {
        ArrayList<Artist> result = new ArrayList<>(from.length());
        for (int i = 0; i < from.length(); i++) {
            result.add(new Artist(from.getJSONObject(i)));
        }
        return result;
    }

    //region implements parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(officialLink);
        dest.writeString(coverSmallLink);
        dest.writeString(coverBigLink);
        dest.writeInt(tracksCount);
        dest.writeInt(albumsCount);
        dest.writeList(genres);
        //dest.writeValue(genres);
        dest.writeList(unknownGenres);
    }

    public static final Parcelable.Creator<Artist> CREATOR = new Parcelable.Creator<Artist>() {
        // распаковываем объект из Parcel
        public Artist createFromParcel(Parcel in) {
            return new Artist(in);
        }

        public Artist[] newArray(int size) {
            return new Artist[size];
        }
    };

    public Artist (Parcel parcel) {
        id              = parcel.readInt();
        name            = parcel.readString();
        description     = parcel.readString();
        officialLink    = parcel.readString();
        coverSmallLink  = parcel.readString();
        coverBigLink    = parcel.readString();
        tracksCount     = parcel.readInt();
        albumsCount     = parcel.readInt();
        //genres          = (List<Genres>)parcel.readValue(null);
        genres = new ArrayList<>();
        parcel.readList(genres, null);
        unknownGenres = new ArrayList<>();
        parcel.readList(unknownGenres, null);
    }
    //endregion

    //region getters
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getOfficialLink() {
        return officialLink;
    }

    public String getCoverSmallLink() {
        return coverSmallLink;
    }

    public String getCoverBigLink() {
        return coverBigLink;
    }

    public int getTracksCount() {
        return tracksCount;
    }

    public int getAlbumsCount() {
        return albumsCount;
    }

    public List<Genres> getGenres() {
        return genres;
    }

    public List<String> getUnknownGenres() {
        return unknownGenres;
    }

    public String getAllGenres() {
        StringBuilder result = new StringBuilder();
        for (Genres genre: genres) {
            result.append(genre);
            result.append(", ");
        }
        for (String genre: unknownGenres) {
            result.append(genre);
            result.append(", ");
        }
        result.delete(result.length() - 2, result.length() - 1);
        return result.toString();
    }
    //endregion

    @Override
    public String toString() {
        return "Artist{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", officialLink='" + officialLink + '\'' +
                ", coverSmallLink='" + coverSmallLink + '\'' +
                ", coverBigLink='" + coverBigLink + '\'' +
                ", tracksCount=" + tracksCount +
                ", albumsCount=" + albumsCount +
                ", genres=" + genres +
                ", unknownGenres=" + unknownGenres +
                '}';
    }
}

